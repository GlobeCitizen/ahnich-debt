package backendserver;

public class ServerCoreConfig {
    private int listenPort;
    private int soTimeout;

    public int getListenPort() {
        return listenPort;
    }

    public int getSoTimeout() {
        return soTimeout;
    }

    public void setListenPort(int listenPort) {
        this.listenPort = listenPort;
    }

    public void setSoTimeout(int soTimeout) {
        this.soTimeout = soTimeout;
    }
}
