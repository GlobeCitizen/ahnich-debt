package backendserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import connectionpool.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;

import static java.lang.Thread.sleep;

public class ClientRequestManager implements Runnable{
    private final static DataSource ds = DataSource.getInstance();
    private static final Logger logger = LoggerFactory.getLogger(ClientRequestManager.class.getName());
    private final InputStream in;
    private final OutputStream out;
    private final static String name = "client-thread-manager";
    private Thread self;
    private Connection c;

    public ClientRequestManager(Socket socket) throws IOException {
        in = socket.getInputStream();
        out = socket.getOutputStream();
        self = new Thread(this, name);
        self.run();
    }

    public void join() throws InterruptedException {
        self.join();
    }

    @Override
    public void run() {
        byte inputData[];
        try{
            while (in.available() == 0){}
            inputData = new byte[in.available()];
            in.read(inputData);
            logger.info("data received {}", new String(inputData));
            final ObjectMapper mapper = new ObjectMapper();
            DataOutputStream dos = new DataOutputStream(out);
            c = ds.getConnection();
            requestManager(mapper, dos, c, inputData);
            ds.putBackConnection(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestManager(ObjectMapper mapper, DataOutputStream dos, Connection c, byte[] inputData) throws Exception {
        HashMap<String, Object> requestMap = mapper.readValue(inputData, HashMap.class);
        logger.info("request : " + requestMap.get("request"));
        if (requestMap.get("request").equals("read")){
            try {
                byte[] response = Crud.select();
                dos.write(response);
                logger.info("read request data sent!");
            }catch(NullPointerException e) {
                byte[] response = String.format("The connection pool is saturated, try again").
                        getBytes(StandardCharsets.UTF_8);
                logger.info("The connection pool is saturated, try again");
                dos.write(response);
            }
        }

        if(requestMap.get("request").equals("insert")){
            String response = Crud.insert((String)requestMap.get("last_name"), (String)requestMap.get("first_name"), (int)requestMap.get("age"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("insert request done");
        }

        if(requestMap.get("request").equals("delete")){
            String response = Crud.delete((String)requestMap.get("last_name"), (String)requestMap.get("first_name"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("delete request done");
        }

        if(requestMap.get("request").equals("update")){
            String response = Crud.update((String)requestMap.get("first_name"), (String)requestMap.get("last_name"), (String) requestMap.get("new_first_name"), (String) requestMap.get("new_last_name"), (int) requestMap.get("new_age"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("update request done");
        }

        if (requestMap.get("request").equals("companylist")) {
            Connection connection = ds.getConnection();
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery("SELECT DISTINCT company_name FROM offices");
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while (rs.next()) {
                HashMap<String, Object> row = new HashMap<>();
                row.put("name", rs.getString("company_name"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));
            logger.info("data sent");
        }

        if(requestMap.get("request").equals("sensorData")){
            int office_id = (int) requestMap.get("space_id");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT * FROM sensor WHERE id_office = ?");
            preparedStatement.setInt(1, office_id);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("id_sensor", rs.getInt("id_sensor"));
                row.put("type", rs.getString("type"));
                row.put("pos_x", rs.getInt("pos_x"));
                row.put("pos_y", rs.getInt("pos_y"));
                row.put("state", rs.getString("state"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));
            logger.info("data sent");
        }

        if(requestMap.get("request").equals("equipmentData")){
            int office_id = (int) requestMap.get("space_id");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT * FROM equipment WHERE id_office = ?");
            preparedStatement.setInt(1, office_id);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("equipment_id", rs.getInt("equipment_id"));
                row.put("type", rs.getString("type"));
                row.put("pos_x", rs.getInt("pos_x"));
                row.put("pos_y", rs.getInt("pos_y"));
                row.put("state", rs.getString("state"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));
            logger.info("data sent");
        }

        if(requestMap.get("request").equals("rentedbuildings")){
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT building_name FROM buildings");
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("building_name", rs.getString("building_name"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));

            logger.info("data sent: " + rowList.toString());
        }

        if(requestMap.get("request").equals("rentedspaces")){
            String building_name = (String) requestMap.get("building_name");
            String company_name = (String) requestMap.get("company_name");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT id_office, type from offices o inner join buildings b on o.id_building = b.id_building where b.building_name = ?  and o.company_name = ? and o.rented=true");
            preparedStatement.setString(1 , building_name);
            preparedStatement.setString(2, company_name);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("space_number", rs.getInt("id_office"));
                row.put("type", rs.getString("type"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));
            logger.info("data sent");
        }

        if (requestMap.get("request").equals("updateSensorPosition")){
            PreparedStatement preparedStatement = c.prepareStatement(
                    "UPDATE sensor SET pos_x=?, pos_y=? WHERE id_sensor=?"
            );
            preparedStatement.setInt(1, (int) requestMap.get("pos_x"));
            preparedStatement.setInt(2, (int) requestMap.get("pos_y"));
            preparedStatement.setInt(3, (int) requestMap.get("id_sensor"));
            int rowsAffected = preparedStatement.executeUpdate();
            logger.info("sensor table {} row updated successfully", rowsAffected);
        }

        if (requestMap.get("request").equals("updateEquipmentPosition")){
            PreparedStatement preparedStatement = c.prepareStatement(
                    "UPDATE equipment SET pos_x=?, pos_y=? WHERE equipment_id=?"
            );
            preparedStatement.setInt(1, (int) requestMap.get("pos_x"));
            preparedStatement.setInt(2, (int) requestMap.get("pos_y"));
            preparedStatement.setInt(3, (int) requestMap.get("equipment_id"));
            int rowsAffected = preparedStatement.executeUpdate();
            logger.info("equipment table {} row updated successfully", rowsAffected);
        }

        if (requestMap.get("request").equals("resetSensorPosition")){
            PreparedStatement preparedStatement = c.prepareStatement(
                    "UPDATE sensor SET pos_x=0, pos_y=0 WHERE id_office=?");
            preparedStatement.setInt(1, (int) requestMap.get("id_space"));
            int rowsAffected = preparedStatement.executeUpdate();
            logger.info("sensor table {} rows updaed successfully", rowsAffected);
        }

        if (requestMap.get("request").equals("resetEquipmentPosition")){
            PreparedStatement preparedStatement = c.prepareStatement(
                    "UPDATE equipment SET pos_x=0, pos_y=0 WHERE id_office=?");
            preparedStatement.setInt(1, (int) requestMap.get("id_space"));
            int rowsAffected = preparedStatement.executeUpdate();
            logger.info("equipment table {} rows updaed successfully", rowsAffected);
        }
    }

    public void start() {
        self.start();
    }
}
