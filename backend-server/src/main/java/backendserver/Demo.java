package backendserver;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class Demo {

    public static ServerConfig config;
    private static final Logger logger = LoggerFactory.getLogger(ClientRequestManager.class.getName());

    public static void main(String[] args) throws ParseException, IOException {

        final Options opts = new Options();
        final Option serverMode = Option.builder().longOpt("serverMode").build();
        opts.addOption(serverMode);

        final CommandLineParser commandLineParser = new DefaultParser();
        final CommandLine cl = commandLineParser.parse(opts, args);

        if(cl.hasOption("serverMode")) {
            logger.info("Server started!");
            config = new ServerConfig();
            Server server = new Server(config);
            server.serve();
        }
    }
}