package backendserver;

public class DataBaseCoreConfig {
    private String driver;
    private String db_url;
    private String db_user;
    private String db_pwd;
    private int min_connection;
    private int max_connection;

    public DataBaseCoreConfig() {
    }

    public String getDriver() {
        return driver;
    }

    public String getDb_url() {
        return db_url;
    }

    public String getDb_user() {
        return db_user;
    }

    public String getDb_pwd() {
        return db_pwd;
    }

    public int getMin_connection() {
        return min_connection;
    }

    public int getMax_connection() {
        return max_connection;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public void setDb_url(String db_url) {
        this.db_url = db_url;
    }

    public void setDb_user(String db_user) {
        this.db_user = db_user;
    }

    public void setDb_pwd(String db_pwd) {
        this.db_pwd = db_pwd;
    }

    public void setMin_connection(int min_connection) {
        this.min_connection = min_connection;
    }

    public void setMax_connection(int max_connection) {
        this.max_connection = max_connection;
    }
}
