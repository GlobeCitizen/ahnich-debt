insert into sensor (type, state, pos_x, pos_y, id_office)
values ('Capteur de presence',false,0,0, :id_office);

insert into sensor (type, state, pos_x, pos_y, id_office)
values('capteur incendie',true,0,0, :id_office);

insert into equipment (type, id_office, pos_x, pos_y, state)
values ('Ecran', :id_office,0,0,false);

insert into equipment (type, id_office, pos_x, pos_y, state)
values ('Fenetre electro-chromatiques' , :id_office,0,0,false);

insert into equipment (type, id_office, pos_x, pos_y, state)
values ('Climatisation', :id_office,0,0,true);

insert into equipment (type, id_office, pos_x, pos_y, state)
values ('Chauffage', :id_office,0,0,true);