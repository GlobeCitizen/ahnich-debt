package pds.client.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;

public class LimitConfig {

    public class Point{
        int x;
        int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    private HashMap<String, Integer> equipmentSensorLimits;

    public LimitConfig(String office_type, String equipmentSensor) throws IOException {
        HashMap<String, HashMap> limits = new ObjectMapper().readValue(getClass().getClassLoader().getResource("limits.json"), HashMap.class);
        HashMap<String, HashMap> officeLimits = limits.get(office_type);
        equipmentSensorLimits = officeLimits.get(equipmentSensor);
    }

    public boolean isInsideRect(Point a, Point b, Point click){
        if (click.getX() > a.getX() && click.getX() < b.getX() && click.getY() > a.getY() && click.getY() < b.getY())
            return true;
        return false;
    }

    public HashMap<String, Integer> getEquipmentSensorLimits() {
        return equipmentSensorLimits;
    }
}
