package pds.client.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MappingFrame extends JFrame {
    public static final String FENETRE_ELECTRO_CHROMATIQUES = "Fenetre electro-chromatiques";
    public static final String BUREAU_FERMÉ = "bureau fermé";
    public static final String ECRAN = "Ecran";
    public static final String CAPTEUR_DE_PRESENCE = "Capteur de presence";
    public static final String CLIMATISATION = "Climatisation";
    public static final String CHAUFFAGE = "Chauffage";
    public static final String SALLE_DE_RÉUNION = "salle de réunion";
    private JPanel mainPanel;
    private JPanel bottomPanel;
    private JPanel topPanel;
    private JComboBox buildings;
    private JComboBox offices;
    private JComboBox sensors;
    private JButton resetButton;
    private JButton backButton;
    private JPanel imagePanel;
    private JButton menuPrincipalButton;
    private JLabel officeNumberLabel;
    private JLabel equipmentSensorLabel;
    private JLabel officeImage;
    private DataGetter dataGetter;
    private DataSetter dataSetter;
    private IconPlacer iconPlacer;
    private String selectedBuilding;
    private String selectedSpace;
    private HashMap<String, Object> selectedSensorEquipement;
    ArrayList<HashMap<String, Object>> spacesList;
    ArrayList<HashMap<String, Object>> sensorsList;
    ArrayList<HashMap<String, Object>> equipmentList;
    List<String> rentedSpaces;
    List<String> sensorToInstall;
    List<String> equipmentToInstall;
    String office_type;
    private String companyName;

    public MappingFrame(String companyName) {
        this.$$$setupUI$$$();
        this.companyName = companyName;
        dataGetter = new DataGetter();
        dataSetter = new DataSetter();
        officeImage = new JLabel();
        getContentPane().add(mainPanel);
        setSize(800, 600);
        imagePanel.add(officeImage);
        officeImage.setLayout(new GridLayout());
        try {
            initCombolist(dataGetter);
            resetButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    iconPlacer.resetIcons();
                    dataSetter.resetSensorPosition(Integer.parseInt(selectedSpace));
                    dataSetter.resetEquipmentPosition(Integer.parseInt(selectedSpace));
                    updateSensorList();
                    updateEquipmentList();
                    equipmentSensorLabel.setText("Choisissez un équipement / capteur dans la liste dessus.");
                    repaint();
                }
            });

            menuPrincipalButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    ChooseCompany frame = new ChooseCompany();
                    frame.setVisible(true);
                    dispose();
                }
            });

            officeImage.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    boolean checkPos = false;
                    try {
                        checkPos = checkPosition(officeImage, (String) selectedSensorEquipement.get("type"), e);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    String message;
                    try {
                        if (e.getButton() == MouseEvent.BUTTON1) {
                            if (checkPos) {
                                if (selectedSensorEquipement.get("id_sensor") != null) {
                                    dataSetter.updateSensorPosition((int) selectedSensorEquipement.get("id_sensor"), e.getX(), e.getY());
                                } else
                                    dataSetter.updateEquipmentPosition((int) selectedSensorEquipement.get("equipment_id"), e.getX(), e.getY());
                                selectedSensorEquipement.replace("pos_x", e.getX());
                                selectedSensorEquipement.replace("pos_y", e.getY());
                                iconPlacer.placeIcon(selectedSensorEquipement, e.getX(), e.getY());
                                sensorToInstall.remove(selectedSensorEquipement.get("type"));
                                sensors.setModel(new DefaultComboBoxModel<String>(sensorToInstall.toArray(new String[0])));
                                //selectedSensorEquipement = selectSensorEquipement((String) sensors.getItemAt(0));
                                if (sensorToInstall.size() == 0) {
                                    equipmentSensorLabel.setText("Aucun équipement/capteur à positionner!");
                                }
                            } else {
                                message = "Vous ne pouvez pas placer " + selectedSensorEquipement.get("type") + " ici!\n Veuillez choisir un endroit adéquat.";
                                JOptionPane.showMessageDialog(officeImage, message);
                            }
                        }
                    } catch (IOException ioException) {
                        ioException.printStackTrace();
                    }

                }
            });

            buildings.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JComboBox cb = (JComboBox) e.getSource();
                    selectedBuilding = (String) cb.getSelectedItem();
                    updateSpaceList();
                }
            });

            offices.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JComboBox cb = (JComboBox) e.getSource();
                    selectedSpace = (String) cb.getSelectedItem();
                    officeNumberLabel.setText("Vous avez choisi le bureau N° : " + selectedSpace + " dans le bâtiment : " + selectedBuilding);
                    updateSensorList();
                    updateEquipmentList();
                    for (HashMap<String, Object> space : spacesList) {
                        if (space.get("space_number").toString().equals(selectedSpace)) {
                            office_type = (String) space.get("type");
                        }
                    }

                    updateOfficeImage();
                    iconPlacer = new IconPlacer(officeImage, sensorsList);
                    iconPlacer.setSensorList(sensorsList);
                    iconPlacer.resetIcons();
                    iconPlacer.placeAllIcons(sensorsList);
                    repaint();
                }
            });

            sensors.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    JComboBox cb = (JComboBox) e.getSource();
                    String sensorEquipementType = (String) cb.getSelectedItem();
                    selectedSensorEquipement = selectSensorEquipement(sensorEquipementType);
                    if (!selectedSensorEquipement.isEmpty()) {
                        equipmentSensorLabel.setText("Choix d'équipement / capteur à positionner : " + sensorEquipementType);
                    }
                }
            });

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private HashMap<String, Object> selectSensorEquipement(String sensorEquipementType) {
        for (HashMap<String, Object> sensorEquipement : sensorsList) {
            if (sensorEquipement.get("type").equals(sensorEquipementType) && (int) sensorEquipement.get("pos_x") == 0 &&
                    (int) sensorEquipement.get("pos_y") == 0) {
                return sensorEquipement;
            }
        }
        return null;
    }

    private void initCombolist(DataGetter dataGetter) throws IOException {
        ArrayList<HashMap<String, Object>> buildingsList = dataGetter.rentedBuildings();
        List<String> rentedBuildings = new ArrayList<>();
        for (HashMap<String, Object> building : buildingsList) {
            rentedBuildings.add((String) building.get("building_name"));
        }

        selectedBuilding = rentedBuildings.get(0);
        spacesList = dataGetter.rentedSpaces(selectedBuilding, companyName);
        rentedSpaces = new ArrayList<>();
        for (HashMap<String, Object> space : spacesList) {
            rentedSpaces.add((String) space.get("space_number").toString());
        }

        selectedSpace = rentedSpaces.get(0);
        for (HashMap<String, Object> space : spacesList) {
            if (space.get("space_number").toString().equals(selectedSpace)) {
                office_type = (String) space.get("type");
            }
        }
        updateOfficeImage();
        sensorsList = dataGetter.sensorData(Integer.parseInt(selectedSpace));
        sensorToInstall = new ArrayList<>();
        for (HashMap<String, Object> sensor : sensorsList) {
            sensorToInstall.add((String) sensor.get("type").toString());
        }

        equipmentList = dataGetter.equipmentData(Integer.parseInt(selectedSpace));
        equipmentToInstall = new ArrayList<>();
        for (HashMap<String, Object> equipment : equipmentList) {
            equipmentToInstall.add((String) equipment.get("type").toString());
        }

        buildings.setModel(new DefaultComboBoxModel<String>(rentedBuildings.toArray(new String[0])));
        offices.setModel(new DefaultComboBoxModel<String>(rentedSpaces.toArray(new String[0])));
        sensorToInstall.addAll(equipmentToInstall);
        sensors.setModel(new DefaultComboBoxModel<String>(sensorToInstall.toArray(new String[0])));

    }

    private void updateSpaceList() {
        try {
            spacesList = dataGetter.rentedSpaces(selectedBuilding, companyName);
            rentedSpaces.clear();
            for (HashMap<String, Object> space : spacesList) {
                rentedSpaces.add((String) space.get("space_number").toString());
            }
            offices.setModel(new DefaultComboBoxModel<String>(rentedSpaces.toArray(new String[0])));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateSensorList() {
        try {
            sensorsList = dataGetter.sensorData(Integer.parseInt(selectedSpace));
            sensorToInstall.clear();
            for (HashMap<String, Object> sensor : sensorsList) {
                if ((int) sensor.get("pos_x") == 0 && (int) sensor.get("pos_y") == 0) {
                    sensorToInstall.add((String) sensor.get("type"));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateEquipmentList() {
        try {
            equipmentList = dataGetter.equipmentData(Integer.parseInt(selectedSpace));
            equipmentToInstall.clear();
            for (HashMap<String, Object> equipment : equipmentList) {
                if ((int) equipment.get("pos_x") == 0 && (int) equipment.get("pos_y") == 0) {
                    equipmentToInstall.add((String) equipment.get("type"));
                }
            }
            sensorToInstall.addAll(equipmentToInstall);
            sensorsList.addAll(equipmentList);
            sensors.setModel(new DefaultComboBoxModel<String>(sensorToInstall.toArray(new String[0])));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateOfficeImage() {
        if (office_type.equals(BUREAU_FERMÉ))
            officeImage.setIcon(new ImageIcon(getClass().getClassLoader().getResource("office.png")));
        else
            officeImage.setIcon(new ImageIcon(getClass().getClassLoader().getResource("reunion.png")));

        imagePanel.add(officeImage);
        officeImage.setLayout(null);
        repaint();
    }

    public Boolean checkPosition(JLabel officeImage, String sensorEquipmentType, MouseEvent event) throws IOException {
        int x = event.getX();
        int y = event.getY();
        int officeWidth = officeImage.getIcon().getIconWidth();
        int officeHeight = officeImage.getIcon().getIconHeight();
        Boolean checkPos = true;
        LimitConfig limitConfig = new LimitConfig(office_type, sensorEquipmentType);
        HashMap<String, Integer> limits = limitConfig.getEquipmentSensorLimits();
        if (office_type.equals(BUREAU_FERMÉ)) {
            switch (sensorEquipmentType) {
                case FENETRE_ELECTRO_CHROMATIQUES:
                    if (!(x > limits.get("x1") && ((y > limits.get("y1") && y < limits.get("y2")) || (y > limits.get("y3") && y < limits.get("y4"))))) {
                        checkPos = false;
                    }
                    break;
                case ECRAN:
                    if (!((x > limits.get("x1") && x < limits.get("x2") && y > limits.get("y1") && y < limits.get("y2")) || (x > limits.get("x3") && x < limits.get("x4") && y > limits.get("y3") && y < limits.get("y4")))) {
                        checkPos = false;
                    }
                    break;
                case CAPTEUR_DE_PRESENCE:
                    if (!(x < limits.get("x1") && y < limits.get("y1") && y > limits.get("y2"))) {
                        checkPos = false;
                    }
                    break;
                case CLIMATISATION:
                case CHAUFFAGE:
                    if (!((y < limits.get("y") && x > limits.get("x") && x < officeWidth - 20) || (y > officeHeight - 25 && x > 20 && x < officeWidth - 20)
                            || (x < limits.get("y") && y > limits.get("x") && y < officeHeight - 20) || (x > officeWidth - 25 && y > 20 && y < officeHeight - 20))) {
                        checkPos = false;
                    }
                    break;
            }
        } else if (office_type.equals(SALLE_DE_RÉUNION)) {
            switch (sensorEquipmentType) {
                case FENETRE_ELECTRO_CHROMATIQUES:
                    //x 35 195 y<85
                    if (!(x > limits.get("x1") && ((y > limits.get("y1") && y < limits.get("y2")) || (y > limits.get("y3") && y < limits.get("y4"))))) {
                        checkPos = false;
                    }
                    break;
                case ECRAN:
                    if (!((x > limits.get("x1") && x < limits.get("x2") && y > limits.get("y1") && y < limits.get("y2")))) {
                        checkPos = false;
                    }
                    break;
                case CAPTEUR_DE_PRESENCE:
                    if (!(x > limits.get("x1") && x < limits.get("x2") && y < limits.get("y1"))) {
                        checkPos = false;
                    }
                    break;
                case CLIMATISATION:
                case CHAUFFAGE:
                    if (!((y < limits.get("y") && x > limits.get("x") && x < officeWidth - 20) || (y > officeHeight - 25 && x > 20 && x < officeWidth - 20)
                            || (x < limits.get("y") && y > limits.get("x") && y < officeHeight - 20) || (x > officeWidth - 25 && y > 20 && y < officeHeight - 20))) {
                        checkPos = false;
                    }
                    break;
            }
        }
        return checkPos;
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final MappingFrame wnd = new MappingFrame("Google");
                try {
                    for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                        if ("Nimbus".equals(info.getName())) {
                            UIManager.setLookAndFeel(info.getClassName());
                            break;
                        }
                    }
                    wnd.setVisible(true);
                } catch (Exception e) {
                    // If Nimbus is not available, you can set the GUI to another look and feel.
                }
            }
        });

    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(3, 1, new Insets(0, 0, 0, 0), -1, -1));
        bottomPanel = new JPanel();
        bottomPanel.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(bottomPanel, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        menuPrincipalButton = new JButton();
        menuPrincipalButton.setText("Menu principal");
        bottomPanel.add(menuPrincipalButton, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        topPanel = new JPanel();
        topPanel.setLayout(new GridLayoutManager(6, 5, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(topPanel, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        buildings = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel1 = new DefaultComboBoxModel();
        buildings.setModel(defaultComboBoxModel1);
        topPanel.add(buildings, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        offices = new JComboBox();
        topPanel.add(offices, new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        sensors = new JComboBox();
        final DefaultComboBoxModel defaultComboBoxModel2 = new DefaultComboBoxModel();
        sensors.setModel(defaultComboBoxModel2);
        topPanel.add(sensors, new GridConstraints(3, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        officeNumberLabel = new JLabel();
        officeNumberLabel.setText("Vous avez choisi le bureau N° :");
        topPanel.add(officeNumberLabel, new GridConstraints(2, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Choix du bâtiment :");
        topPanel.add(label1, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Choix du bureau :");
        topPanel.add(label2, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer1 = new Spacer();
        topPanel.add(spacer1, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        topPanel.add(spacer2, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        resetButton = new JButton();
        resetButton.setText("Réinitialiser");
        topPanel.add(resetButton, new GridConstraints(5, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        equipmentSensorLabel = new JLabel();
        equipmentSensorLabel.setText("Choisissez un équipement / capteur dans la liste dessus.");
        topPanel.add(equipmentSensorLabel, new GridConstraints(4, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        imagePanel = new JPanel();
        imagePanel.setLayout(new BorderLayout(0, 0));
        mainPanel.add(imagePanel, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}

