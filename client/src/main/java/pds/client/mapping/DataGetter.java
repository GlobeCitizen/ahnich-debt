package pds.client.mapping;

import pds.client.ClientConf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataGetter {
    private MappingRequest mappingRequest;
    private ArrayList<HashMap<String, Object>> answer;
    private HashMap<String, Object> request;


    public ArrayList<HashMap<String, Object>> sensorData(int space_id) throws IOException {
        mappingRequest = new MappingRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "sensorData");
        request.put("space_id", space_id);
        answer = mappingRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> equipmentData(int space_id) throws IOException {
        mappingRequest = new MappingRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "equipmentData");
        request.put("space_id", space_id);
        answer = mappingRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> rentedBuildings() throws IOException {
        mappingRequest = new MappingRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "rentedbuildings");
        answer = mappingRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> rentedSpaces(String building_name, String companyName) throws IOException {
        mappingRequest = new MappingRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "rentedspaces");
        request.put("building_name", building_name);
        request.put("company_name", companyName);
        answer = mappingRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> companyData() throws IOException {
        mappingRequest = new MappingRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "companylist");
        answer = mappingRequest.start(request);
        return(answer);
    }

}
