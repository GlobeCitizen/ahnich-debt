package pds.client.mapping;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;


public class IconPlacer {
    private JLabel officeImage;
    private HashMap<String, ImageIcon> icons;
    private HashMap<String, JLabel> iconsToPlace;
    final int iconSize = 40;
    private ArrayList<HashMap<String, Object>> sensorList;
    String message;

    public void setSensorList(ArrayList<HashMap<String, Object>> sensorList) {
        this.sensorList = sensorList;
    }

    public IconPlacer(JLabel officeImage, ArrayList<HashMap<String, Object>> sensorList) {
        this.officeImage = officeImage;
        initIconList();
        initIconsToPlaceList(sensorList);
    }

    public void initIconList(){
        icons = new HashMap<>();
        icons.put("Ecran", new ImageIcon(
                new ImageIcon(getClass().getClassLoader().getResource("tv.png")).getImage()
                        .getScaledInstance(iconSize, iconSize, Image.SCALE_SMOOTH)));
        icons.put("Capteur de presence", new ImageIcon(
                new ImageIcon(getClass().getClassLoader().getResource("presence.png")).getImage()
                        .getScaledInstance(iconSize, iconSize, Image.SCALE_SMOOTH)));
        icons.put("capteur incendie", new ImageIcon(
                new ImageIcon(getClass().getClassLoader().getResource("incendie.png")).getImage()
                        .getScaledInstance(iconSize, iconSize, Image.SCALE_SMOOTH)));
        icons.put("Fenetre electro-chromatiques", new ImageIcon(
                new ImageIcon(getClass().getClassLoader().getResource("window.png")).getImage()
                        .getScaledInstance(iconSize, iconSize, Image.SCALE_SMOOTH)));
        icons.put("Chauffage", new ImageIcon(
                new ImageIcon(getClass().getClassLoader().getResource("heating.png")).getImage()
                        .getScaledInstance(iconSize, iconSize, Image.SCALE_SMOOTH)));
        icons.put("Climatisation", new ImageIcon(
                new ImageIcon(getClass().getClassLoader().getResource("air.png")).getImage()
                        .getScaledInstance(iconSize, iconSize, Image.SCALE_SMOOTH)));
    }

    public void initIconsToPlaceList(ArrayList<HashMap<String, Object>> sensorList){
        iconsToPlace = new HashMap<>();
        for (HashMap<String, Object> sensorEquipement: sensorList) {
            if (sensorEquipement.get("id_sensor") != null) {
                String sensorEquipmentTypeId = sensorEquipement.get("id_sensor" ) + "-"
                        + sensorEquipement.get("type");
                iconsToPlace.put(sensorEquipmentTypeId, new JLabel(icons.get(sensorEquipement.get("type"))));
            }else {
                String sensorEquipmentTypeId = sensorEquipement.get("equipment_id") + "-"
                        + sensorEquipement.get("type");
                iconsToPlace.put(sensorEquipmentTypeId, new JLabel(icons.get(sensorEquipement.get("type"))));
            }
        }
    }

    public void placeIcon(HashMap<String, Object> equipmentSensor, int x, int y){
        JLabel icon;

        if (equipmentSensor.get("id_sensor") != null) {
            String sensorEquipmentTypeId = equipmentSensor.get("id_sensor" ) + "-"
                    + equipmentSensor.get("type");
            icon = iconsToPlace.get(sensorEquipmentTypeId);
        }else{
            String sensorEquipmentTypeId = equipmentSensor.get("equipment_id") + "-"
                    + equipmentSensor.get("type");
            icon = iconsToPlace.get(sensorEquipmentTypeId);
        }
        int iconHeight = icon.getIcon().getIconHeight();
        int iconWidth = icon.getIcon().getIconWidth();
        officeImage.add(icon);
        icon.setBounds(x-iconSize/2, y-iconSize/2, iconWidth, iconHeight);
        if (icon.getListeners(MouseListener.class).length == 0)
            icon.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    super.mouseClicked(e);
                    if (e.getButton() == MouseEvent.BUTTON3) {
                        message = getState(equipmentSensor);
                        JOptionPane.showMessageDialog(officeImage, message);
                    }
                }
            });
    }

    public void resetIcons(){
        for (Component c:officeImage.getComponents()) {
            officeImage.remove(c);
        }
    }

    public void placeAllIcons(ArrayList<HashMap<String, Object>> sensorList) {
        for (HashMap<String, Object> sensorEquipment: sensorList) {
            if((int)sensorEquipment.get("pos_x")!=0 && (int)sensorEquipment.get("pos_y")!=0) {
                placeIcon(sensorEquipment,(int) sensorEquipment.get("pos_x"), (int) sensorEquipment.get("pos_y"));
            }
        }
    }

    public String getState(HashMap<String, Object> equipmentSensor){
        if (equipmentSensor.get("id_sensor") != null) {
            if (equipmentSensor.get("state").equals("t"))
                message = "le flux de ce capteur est opérationnel";
            else
                message = "le flux des données de ce captuer n'est pas opérationnel, vérifiez le!";
        }else{
            if (equipmentSensor.get("state").equals("t"))
                message = "Cet equipement fonctionne correctement.";
            else
                message = "Cet équipement est hors service, vérifiez le!";
        }
        return message;
    }
}
