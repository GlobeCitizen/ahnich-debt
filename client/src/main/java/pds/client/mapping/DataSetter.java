package pds.client.mapping;

import pds.client.ClientConf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataSetter {
    private MappingRequest mappingRequest;
    private ArrayList<HashMap<String, Object>> answer;
    private HashMap<String, Object> request;

    public void updateSensorPosition(int id, int x, int y) throws IOException {
        mappingRequest = new MappingRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "updateSensorPosition");
        request.put("id_sensor", id);
        request.put("pos_x", x);
        request.put("pos_y", y);
        mappingRequest.startUpdate(request);
    }

    public void updateEquipmentPosition(int id, int x, int y) throws IOException {
        mappingRequest = new MappingRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "updateEquipmentPosition");
        request.put("equipment_id", id);
        request.put("pos_x", x);
        request.put("pos_y", y);
        mappingRequest.startUpdate(request);
    }

    public void resetSensorPosition(int space_id){
        try {
            mappingRequest = new MappingRequest(new ClientConf());
        } catch (IOException e) {
            e.printStackTrace();
        }
        request = new HashMap<>();
        request.put("request", "resetSensorPosition");
        request.put("id_space", space_id);
        mappingRequest.startUpdate(request);
    }

    public void resetEquipmentPosition(int space_id) {
        try {
            mappingRequest = new MappingRequest(new ClientConf());
        } catch (IOException e) {
            e.printStackTrace();
        }
        request = new HashMap<>();
        request.put("request", "resetEquipmentPosition");
        request.put("id_space", space_id);
        mappingRequest.startUpdate(request);
    }
}

