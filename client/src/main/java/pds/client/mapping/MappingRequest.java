package pds.client.mapping;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pds.client.ClientConf;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;


public class MappingRequest implements Serializable {
    private static final Logger logger = LoggerFactory.getLogger(pds.client.mapping.MappingRequest.class.getName());
    private static String clientRequestFileLocation;
    private static String clientRequestEnvVar = "JSON_CONF";
    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;

    public MappingRequest(final ClientConf clientConf) {
        try {
            socket = new Socket(clientConf.getConfig().getIpAddress(), clientConf.getConfig().getListenPort());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<HashMap<String, Object>> start(HashMap<String, Object> map) throws IOException {
        final ObjectMapper mapper = new ObjectMapper();
        byte[] inputData;
        try {
            inputStream = socket.getInputStream();
            outputStream = socket.getOutputStream();
            outputStream.write(mapper.writeValueAsBytes(map));
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("Request submitted");
        while(inputStream.available() == 0) {}
        inputData = new byte[inputStream.available()];
        inputStream.read(inputData);
        ArrayList<HashMap<String, Object>> arrayAnswer = mapper.readValue(inputData, ArrayList.class);
        logger.info("data received : {}", arrayAnswer);
        return(arrayAnswer);
    }

    public void startUpdate(HashMap<String, Object> map) {
        final ObjectMapper mapper = new ObjectMapper();
        try {
            outputStream = socket.getOutputStream();
            outputStream.write(mapper.writeValueAsBytes(map));
        } catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("Request submitted");
    }
}