package pds.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ClientConf {
    private final static Logger logger = LoggerFactory.getLogger(ClientConf.class.getName());
    private final static String clientConfigEnvVar = "CLIENT_CONF";
    private String clientConfLocation;
    private ClientCoreConf config;

    public ClientConf() throws IOException {
        clientConfLocation = System.getenv(clientConfigEnvVar);
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        config = mapper.readValue(new File(clientConfLocation), ClientCoreConf.class);
    }

    public ClientCoreConf getConfig() {
        return config;
    }
}
